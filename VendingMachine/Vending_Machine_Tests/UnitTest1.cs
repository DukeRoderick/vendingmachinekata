﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingController;
using VendingChange;
using VendingInventory;

namespace Vending_Machine_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestQuarter()
        {
            ICoin quarter = new Quarter();
            Assert.AreEqual(.955f, quarter.Size);
            Assert.AreEqual(5.670f, quarter.Weight);
        }
        [TestMethod]
        public void TestDime()
        {
            ICoin dime = new Dime();
            Assert.AreEqual(2.268f, dime.Weight);
            Assert.AreEqual(0.705f, dime.Size);
        }
        [TestMethod]
        public void TestNickel()
        {
            ICoin nickel = new Nickel();
            Assert.AreEqual(5.000f, nickel.Weight);
            Assert.AreEqual(0.835f, nickel.Size);
        }
        [TestMethod]
        public void TestPenny()
        {
            ICoin penny = new Penny();
            Assert.AreEqual(2.500f, penny.Weight);
            Assert.AreEqual(0.750f, penny.Size);
        }

        [TestMethod]
        public void TestNoChangeVendingMachine()
        {
            VendingMachine vendingMachine = new VendingMachine();
            Assert.AreEqual(0.0m, vendingMachine.GetTotalStoredValue());
        }

        [TestMethod]
        public void TestOneDollarTwentyFiveCentsChangeVendingMachine()
        {
            VendingMachine vendingMachine = new VendingMachine(new CoinTray(5, 0, 0, 0));
            Assert.AreEqual(1.25m, vendingMachine.GetTotalStoredValue());
        }

        [TestMethod]
        public void AddQuarter()
        {
            VendingMachine vendingMachine = new VendingMachine();
            CoinTray expectedChange = new CoinTray(1, 0, 0, 0);
            Assert.AreEqual(expectedChange, vendingMachine.AddChange(new Quarter()).InsertedCoins);
        }

        [TestMethod]
        public void AddDime()
        {
            VendingMachine vendingMachine = new VendingMachine();
            CoinTray expectedChange = new CoinTray(0, 1, 0, 0);
            Assert.AreEqual(expectedChange, vendingMachine.AddChange(new Dime()).InsertedCoins);
        }

        [TestMethod]
        public void AddNickel()
        {
            VendingMachine vendingMachine = new VendingMachine();
            CoinTray expectedChange = new CoinTray(0, 0, 1, 0);
            Assert.AreEqual(expectedChange, vendingMachine.AddChange(new Nickel()).InsertedCoins);
        }

        [TestMethod]
        public void AddPenny()
        {
            VendingMachine vendingMachine = new VendingMachine();
            CoinTray expectedChange = new CoinTray(0, 0, 0, 1);
            Assert.AreEqual(false, vendingMachine.AddChange(new Penny()).Success);
        }

        [TestMethod]
        public void ValuesFromCoins()
        {
            VendingMachine vendingMachine = new VendingMachine();
            CoinTray coins = new CoinTray(2, 2, 1, 0);
            Assert.AreEqual(.75m, vendingMachine.GetDecimalValueOfCoins(coins));
        }

        [TestMethod]
        public void MakeChange_ThirtySevenCents()
        {
            VendingMachine vendingMachine = new VendingMachine();
            decimal coinValue = vendingMachine.GetDecimalValueOfCoins(vendingMachine.MakeChange(new CoinTray(4, 0, 0, 0), .63m));
            Assert.AreEqual(.37m, coinValue);
        }

        [TestMethod]
        public void MakeChange_ThirySevenCentsInCoins()
        {
            VendingMachine vendingMachine = new VendingMachine();
            CoinTray coins = new CoinTray(1, 1, 0, 2);
            Assert.AreEqual(.37m, vendingMachine.GetDecimalValueOfCoins(coins));
            Assert.AreEqual(coins, vendingMachine.MakeChange(new CoinTray(4, 0, 0, 0), .63m));
        }

        [TestMethod]
        public void ReturnChange()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Dime());
            CoinTray expectedCoinTray = new CoinTray(1, 1, 0, 0);
            Assert.AreEqual(expectedCoinTray, vendingMachine.GetCurrenInsertedCoins());
            vendingMachine.ReturnChange();
            Assert.AreEqual(expectedCoinTray, vendingMachine.CheckCoinsInReturnSlot());
            Assert.AreEqual(new CoinTray(), vendingMachine.GetCurrenInsertedCoins());
        }

        [TestMethod]
        public void ReturnChangeFail()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Dime());
            vendingMachine.ReturnChange();
            vendingMachine.AddChange(new Dime());
            vendingMachine.ReturnChange();
            Assert.AreNotEqual(.35m, vendingMachine.GetDecimalValueOfCoins(vendingMachine.CheckCoinsInReturnSlot()));
            Assert.AreEqual(.45m, vendingMachine.GetDecimalValueOfCoins(vendingMachine.CheckCoinsInReturnSlot()));
            vendingMachine.EmptyCoinReturn();
            Assert.AreEqual(new CoinTray(), vendingMachine.CheckCoinsInReturnSlot());
        }

        [TestMethod]
        public void TestInsertedCoinValue()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Dime());
            vendingMachine.AddChange(new Nickel());
            decimal expectedValue = .40m;
            Assert.AreEqual(expectedValue, vendingMachine.GetInsertedStoreValue());
        }

        [TestMethod]
        public void TestPurchase()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Quarter());
            CoinTray change = vendingMachine.MakeChange(vendingMachine.GetCurrenInsertedCoins(), .50m);
            vendingMachine.AdjustCoinsAfterPurchase(change);
            Assert.AreEqual(new CoinTray(), vendingMachine.GetCurrenInsertedCoins());
            Assert.AreEqual(new CoinTray(1, 0, 0, 0), vendingMachine.CheckCoinsInReturnSlot());
            Assert.AreEqual(new CoinTray(2, 0, 0, 0), vendingMachine.GetCurrentStoredCoins());
        }

        [TestMethod]
        public void TestFullPurchaseProcess()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddToInvetory(VendingItems.Chips, 1);
            vendingMachine.AddToInvetory(VendingItems.Cola, 1);
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Quarter());
            SelectItemResponse response = vendingMachine.PurchaseItem(VendingItems.Chips);
            Assert.AreEqual(true, response.Success);
            Assert.AreEqual(VendingMachine.THANK_YOU, response.Message);
            response = vendingMachine.PurchaseItem(VendingItems.Cola);
            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(VendingMachine.PRICE + ": " + vendingMachine.GetPriceOfInventoryItem(VendingItems.Cola), response.Message);
        }
        [TestMethod]
        public void TestSoldOut()
        {
            VendingMachine vendingMachine = new VendingMachine();
            SelectItemResponse response = vendingMachine.PurchaseItem(VendingItems.Chips);
            Assert.AreEqual(false, response.Success);
            Assert.AreEqual(VendingMachine.SOLD_OUT, response.Message);
        }
        [TestMethod]
        public void TestSoldOutAfterPurchase()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddChange(new Quarter());
            vendingMachine.AddToInvetory(VendingItems.Chips, 1);
            SelectItemResponse response = vendingMachine.PurchaseItem(VendingItems.Chips);
            Assert.AreEqual(true, response.Success);
            response = vendingMachine.PurchaseItem(VendingItems.Chips);
            Assert.AreEqual(VendingMachine.SOLD_OUT, response.Message);
        }

        [TestMethod]
        public void TestPriceOfSoda()
        {
            VendingMachine vendingMachine = new VendingMachine();
            Assert.AreEqual(1.00m, vendingMachine.GetPriceOfInventoryItem(VendingItems.Cola));
        }

        [TestMethod]
        public void TestPriceOfChips()
        {
            VendingMachine vendingMachine = new VendingMachine();
            Assert.AreEqual(.5m, vendingMachine.GetPriceOfInventoryItem(VendingItems.Chips));
        }

        [TestMethod]
        public void TestPriceOfCandy()
        {
            VendingMachine vendingMachine = new VendingMachine();
            Assert.AreEqual(.65m, vendingMachine.GetPriceOfInventoryItem(VendingItems.Candy));
        }

        [TestMethod]
        public void ItemIsSoldOut()
        {
            VendingMachine vendingMachine = new VendingMachine();
            Assert.AreEqual(false, vendingMachine.HasInventoryOfItem(VendingItems.Cola));
        }

        [TestMethod]
        public void ItemIsNotSoldOut()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddToInvetory(VendingItems.Cola, 5);
            Assert.AreEqual(true, vendingMachine.HasInventoryOfItem(VendingItems.Cola));
        }

        [TestMethod]
        public void InventoryOfItem()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AddToInvetory(VendingItems.Cola, 5);
            Assert.AreEqual(5, vendingMachine.GetInventoryOfItem(VendingItems.Cola));
        }

        [TestMethod]
        public void DisplaysPriceOfItem()
        {
            VendingMachine vendingMachine = new VendingMachine();
        }
    }
}
