﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendingInventory;
using VendingChange;

namespace VendingMachineMain
{
    class Program
    {
        private static VendingController.VendingMachine vendingMachine;
        static void Main(string[] args)
        {
            vendingMachine = new VendingController.VendingMachine();
            SetInventoryOfVendingMachine();
            Intro();
        }

        private static void SetInventoryOfVendingMachine()
        {
            vendingMachine.AddToInvetory(VendingItems.Cola, 20);
            vendingMachine.AddToInvetory(VendingItems.Chips, 20);
            vendingMachine.AddToInvetory(VendingItems.Candy, 20);
        }

        private static void Intro()
        {
            DisplayInventory();
            MainMenuOptions();
        }

        private static void MainMenuOptions()
        {
            Console.WriteLine("\nPlease enter the number for the appropriate option.");
            Console.WriteLine("\nINSERTED COINS: " + vendingMachine.GetInsertedStoreValue());
            Console.WriteLine("\n1. INSERT COINS");
            Console.WriteLine("\n2. Look at available items");
            ReadInput(Console.ReadLine());
        }

        private static void ReadInput(string input)
        {
            //Using LINQ statement for readability. Could have used RegEx.
            if (input.Any(x => !char.IsDigit(x)))
            {
                Console.Clear();               
                DisplayInventory();
                Console.WriteLine("\nInvalid Character Detected! Please try again.");
                MainMenuOptions();
            }
            else
            {
                Console.Clear();
                switch(int.Parse(input))
                {
                    case 1:
                        AddMoneyMenu();
                        break;
                    case 2:
                        LookAtAvailableItems();
                        break;
                    case 3:
                        break;
                }
            }
        }

        private static void AddMoneyMenu()
        {
            Console.WriteLine("INSERT COINS");
            Console.WriteLine("\n1. Add Quarter");
            Console.WriteLine("2. Add Dime");
            Console.WriteLine("3. Add Nickel");
            Console.WriteLine("4. Add Penny");
            Console.WriteLine("\nPlease enter the number for the appropriate option.");
            string input = Console.ReadLine();
            if (input.Any(x => !char.IsDigit(x)))
            {
                Console.Clear();
                Console.WriteLine("\nInvalid Character Detected! Please try again.");
                AddMoneyMenu();
            }
            else
            {
                VendingController.AddCoinResponse response = null;
                Console.Clear();
                ICoin coinToBeAdded = null;
                switch (int.Parse(input))
                {
                    case 1:
                        coinToBeAdded = new Quarter();
                        break;
                    case 2:
                        coinToBeAdded = new Dime();
                        break;
                    case 3:
                        coinToBeAdded = new Nickel();
                        break;
                    case 4:
                        coinToBeAdded = new Penny();
                        break;
                }
                response = vendingMachine.AddChange(coinToBeAdded);
                if (response.Success == false)
                    Console.WriteLine("\n" + response.ErrorResponse);
                else
                    Console.WriteLine("\nCurrently Inserted Money Value: " + vendingMachine.GetInsertedStoreValue());
                Console.WriteLine("\nPlease click any button to continue back to the main menu...");
                Console.ReadLine();
                Console.Clear();
                MainMenuOptions();
            }
        }

        private static void LookAtAvailableItems()
        {
            Console.WriteLine("Currently Inserted Money Value: " + vendingMachine.GetInsertedStoreValue());
            Console.WriteLine("\n1. Select " + VendingItems.Cola);
            Console.WriteLine("\n2. Select " + VendingItems.Chips);
            Console.WriteLine("\n3. Select " + VendingItems.Candy);
            Console.WriteLine("\n4. Return to main menu");
            Console.WriteLine("\nPlease enter the number for the appropriate option.");
            string input = Console.ReadLine();
            if (input.Any(x => !char.IsDigit(x)))
            {
                Console.Clear();
                Console.WriteLine("Invalid Character Detected! Please try again.\n");
                LookAtAvailableItems();
            }
            else
            {
                Console.Clear();           
                switch (int.Parse(input))
                {
                    case 1:
                        if(vendingMachine.GetInsertedStoreValue() < vendingMachine.GetPriceOfInventoryItem(VendingItems.Cola))
                        {

                        }
                        // + " | $" + vendingMachine.GetPriceOfInventoryItem(VendingItems.Cola)
                        break;
                    case 2:
                        // + " | $" + vendingMachine.GetPriceOfInventoryItem(VendingItems.Chips)
                        break;
                    case 3:
                        // + " | $" + vendingMachine.GetPriceOfInventoryItem(VendingItems.Candy)
                        break;
                    case 4:
                        MainMenuOptions();
                        break;
                }
            }
        }

        private static void DisplayInventory()
        {
            Console.WriteLine(VendingItems.Cola + ": " + vendingMachine.GetInventoryOfItem(VendingItems.Cola));
            Console.WriteLine(VendingItems.Chips + ": " + vendingMachine.GetInventoryOfItem(VendingItems.Chips));
            Console.WriteLine(VendingItems.Candy + ": " + vendingMachine.GetInventoryOfItem(VendingItems.Candy));
        }
    }
}
