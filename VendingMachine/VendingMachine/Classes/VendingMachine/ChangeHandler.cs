﻿using System;


namespace VendingController
{
    public partial class VendingMachine
    {
        private class ChangeHandler
        {
            private float lenianceEpsilon = .01f;
            #region Quarter
            public const float EXPECTED_QUARTER_WEIGHT = 5.670f;
            public const float EXPECTED_QUARTER_SIZE = 0.955f;
            public const decimal QUARTER_VALUE = .25m;
            #endregion
            #region Dime
            public const float EXPECTED_DIME_WEIGHT = 2.268f;
            public const float EXPECTED_DIME_SIZE = 0.705f;
            public const decimal DIME_VALUE = .10m;
            #endregion
            #region Nickel
            public const float EXPECTED_NICKEL_WEIGHT = 5.000f;
            public const float EXPECTED_NICKEL_SIZE = 0.835f;
            public const decimal NICKEL_VALUE = .05m;
            #endregion
            #region Penny
            public const float EXPECTED_PENNY_WEIGHT = 2.500f;
            public const float EXPECTED_PENNY_SIZE = 0.750f;
            public const decimal PENNY_VALUE = .01m;
            #endregion

            public decimal StoredChangeRawValue { get { return GetTotalCoinValue(storedVendingMachineCoinCounts); } }
            public decimal InsertedChangeRawValue { get { return GetTotalCoinValue(insertedVendingMachineCoinCounts); } }

            private int[] storedVendingMachineCoinCounts = new int[4];
            private int[] insertedVendingMachineCoinCounts = new int[4];
            private int[] coinReturnVendingMachineCoinCounts = new int[4];
            private float[] coinSizes = new float[4];
            private float[] coinWeights = new float[4];
            private decimal[] coinValues = new decimal[4];

            public ChangeHandler(CoinTray startingDollarAmount)
            {
                storedVendingMachineCoinCounts[0] = startingDollarAmount.Quarters;
                storedVendingMachineCoinCounts[1] = startingDollarAmount.Dimes;
                storedVendingMachineCoinCounts[2] = startingDollarAmount.Nickels;
                storedVendingMachineCoinCounts[3] = startingDollarAmount.Pennies;

                SetCoinSizeWeightAndValue();
            }

            private void SetCoinSizeWeightAndValue()
            {
                coinSizes[0] = EXPECTED_QUARTER_SIZE;
                coinSizes[1] = EXPECTED_DIME_SIZE;
                coinSizes[2] = EXPECTED_NICKEL_SIZE;
                coinSizes[3] = EXPECTED_PENNY_SIZE;

                coinWeights[0] = EXPECTED_QUARTER_WEIGHT;
                coinWeights[1] = EXPECTED_DIME_WEIGHT;
                coinWeights[2] = EXPECTED_NICKEL_WEIGHT;
                coinWeights[3] = EXPECTED_PENNY_WEIGHT;

                coinValues[0] = QUARTER_VALUE;
                coinValues[1] = DIME_VALUE;
                coinValues[2] = NICKEL_VALUE;
                coinValues[3] = PENNY_VALUE;
            }

            /// <summary>
            /// Adds money to the vending machine
            /// Needs to be updated to add the coin to the correct coin holder
            /// Returns coins in inserted slot on vending machine
            /// </summary>
            /// <param name="coin"></param>
            /// <returns></returns>
            public AddCoinResponse AddChange(VendingChange.ICoin coin)
            {
                string error = string.Empty;
                if(coin is VendingChange.Penny)
                    return new AddCoinResponse(false, new CoinTray(), "This vending machine does not accept pennies.");
                for (int i = 0; i < 4; i++)
                {
                    if(Math.Abs(coin.Weight - coinWeights[i]) < lenianceEpsilon
                        || Math.Abs(coin.Size - coinSizes[i]) < lenianceEpsilon)
                    {
                        insertedVendingMachineCoinCounts[i]++;
                        return new AddCoinResponse(true, GetInsertedCoins(), string.Empty);
                    }
                }
                return new AddCoinResponse(false, new CoinTray(), "Unknown error occurred");
            }

            /// <summary>
            /// Takes in a set of coins as the total given
            /// Converts the coins to a decimal value and subtracts the
            /// cost of the item.
            /// Then goes and generates the proper amount of coins for change
            /// </summary>
            /// <param name="coins"></param>
            /// <param name="cost"></param>
            /// <returns></returns>
            public CoinTray MakeChange(CoinTray coins, decimal cost)
            {
                decimal localChange = ValueFromCoinTray(coins) - cost;
                int quarters = 0;
                int dimes = 0;
                int nickels = 0;
                int pennies = 0;
                while(true)
                {
                    if(localChange - QUARTER_VALUE >= 0)
                    {
                        quarters++;
                        localChange -= QUARTER_VALUE;
                    }
                    else if (localChange - DIME_VALUE >= 0)
                    {
                        dimes++;
                        localChange -= DIME_VALUE;
                    }
                    else if (localChange - NICKEL_VALUE >= 0)
                    {
                        nickels++;
                        localChange -= NICKEL_VALUE;
                    }
                    else if (localChange - PENNY_VALUE >= 0)
                    {
                        pennies++;
                        localChange -= PENNY_VALUE;
                    }
                    if (localChange == 0)
                        break;
                }
                //insertedVendingMachineCoinCounts[0] = quarters;
                //insertedVendingMachineCoinCounts[1] = dimes;
                //insertedVendingMachineCoinCounts[2] = nickels;
                //insertedVendingMachineCoinCounts[3] = pennies;
                return new CoinTray(quarters, dimes, nickels, pennies);
            }

            public void PurchaseItem(CoinTray change)
            {
                int[] newChange = new int[4];
                newChange[0] = change.Quarters;
                newChange[1] = change.Dimes;
                newChange[2] = change.Nickels;
                newChange[3] = change.Pennies;
                int[] coinsToAdd = new int[4];
                for(int i = 0; i < 4; i++)
                {
                    coinsToAdd[i] = insertedVendingMachineCoinCounts[i] - newChange[i];
                    storedVendingMachineCoinCounts[i] += coinsToAdd[i];
                    coinReturnVendingMachineCoinCounts[i] += newChange[i];
                    insertedVendingMachineCoinCounts[i] = 0;
                }
            }

            /// <summary>
            /// Returns all the current coins in the vending machine
            /// </summary>
            /// <returns></returns>
            public CoinTray GetStoredCoins()
            {
                return new CoinTray(storedVendingMachineCoinCounts[0], storedVendingMachineCoinCounts[1], storedVendingMachineCoinCounts[2], storedVendingMachineCoinCounts[3]);
            }

            public CoinTray GetInsertedCoins()
            {
                return new CoinTray(insertedVendingMachineCoinCounts[0],
                            insertedVendingMachineCoinCounts[1],
                            insertedVendingMachineCoinCounts[2],
                            insertedVendingMachineCoinCounts[3]);
            }

            public CoinTray GetCoinReturnCoins()
            {
                return new CoinTray(coinReturnVendingMachineCoinCounts[0],
                            coinReturnVendingMachineCoinCounts[1],
                            coinReturnVendingMachineCoinCounts[2],
                            coinReturnVendingMachineCoinCounts[3]);
            }

            /// <summary>
            /// Returns the coins that were inserted in the vending machine
            /// and adds them to the return slot.
            /// </summary>
            /// <returns></returns>
            public void ReturnCoinsFromInsertedSlot()
            {
                //CoinTray heldCoins = GetInsertedCoins();
                for (int i = 0; i < insertedVendingMachineCoinCounts.Length; i++)
                {
                    coinReturnVendingMachineCoinCounts[i] += insertedVendingMachineCoinCounts[i];
                    insertedVendingMachineCoinCounts[i] = 0;
                }
                //return heldCoins;
            }

            public CoinTray CheckCoinsInCoinReturn()
            {
                CoinTray heldCoins = GetCoinReturnCoins();
                return heldCoins;
            }

            public void EmptyCoinReturn()
            {
                for (int i = 0; i < coinReturnVendingMachineCoinCounts.Length; i++)
                {
                    coinReturnVendingMachineCoinCounts[i] = 0;
                }
            }

            /// <summary>
            /// Returns the decimal value from the given CoinTray
            /// </summary>
            /// <param name="coins"></param>
            /// <returns></returns>
            public decimal ValueFromCoinTray(CoinTray coins)
            {
                decimal total = 0m;
                total += coins.Quarters * QUARTER_VALUE;
                total += coins.Dimes * DIME_VALUE;
                total += coins.Nickels * NICKEL_VALUE;
                total += coins.Pennies * PENNY_VALUE;
                return total;
            }

            /// <summary>
            /// Returns the total value of the coins in the vending machine
            /// </summary>
            /// <returns></returns>
            private decimal GetTotalCoinValue(int[] coinStore)
            {
                decimal total = 0m;
                for (int i = 0; i < 4; i++)
                {
                    total += coinStore[i] * coinValues[i];
                }
                return total;
            }        
        }
    }
}
