﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingController
{
    public class SelectItemResponse
    {
        private bool success = false;
        private string message = string.Empty;

        public bool Success { get { return success; } }
        public string Message { get { return message; } }

        public SelectItemResponse(bool _success, string _message)
        {
            success = _success;
            message = _message;
        }
    }
}
