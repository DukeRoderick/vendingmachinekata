﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingController
{
    public struct CoinTray
    {
        public int Quarters { get; private set; }
        public int Dimes { get; private set; }
        public int Nickels { get; private set; }
        public int Pennies { get; private set; }

        public CoinTray(int quarters, int dimes, int nickels, int pennies)
        {
            Quarters = quarters;
            Dimes = dimes;
            Nickels = nickels;
            Pennies = pennies;
        }
    }
}
