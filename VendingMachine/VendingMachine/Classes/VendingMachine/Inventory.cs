﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendingInventory;
namespace VendingController
{
    public partial class VendingMachine
    {
        private class Inventory
        {
            private Dictionary<string, Tuple<decimal, int>> inventoryItems = new Dictionary<string, Tuple<decimal, int>>();

            public Inventory()
            {
                inventoryItems.Add(VendingItems.Cola, new Tuple<decimal, int>(1m, 0));
                inventoryItems.Add(VendingItems.Chips, new Tuple<decimal, int>(.5m, 0));
                inventoryItems.Add(VendingItems.Candy, new Tuple<decimal, int>(.65m, 0));
            }

            public InventorySearchResponse GetInventoryItem(string item)
            {
                Tuple<decimal, int> valueOfItem = new Tuple<decimal, int>(0m, 0);
                bool foundItem = false; ;
                foundItem = inventoryItems.TryGetValue(item, out valueOfItem);
                return new InventorySearchResponse(foundItem, valueOfItem);
            }

            public void AddInventory(string item, int inventoryCount)
            {
                bool itemFound = false;
                Tuple<decimal, int> valueOfItem = new Tuple<decimal, int>(0m, 0);
                itemFound = inventoryItems.TryGetValue(item, out valueOfItem);
                if (itemFound == true)
                    inventoryItems[item] = new Tuple<decimal, int>(valueOfItem.Item1, inventoryCount);
            }

            public void RemoveInvetory(string item)
            {
                bool itemFound = false;
                Tuple<decimal, int> valueOfItem = new Tuple<decimal, int>(0m, 0);
                itemFound = inventoryItems.TryGetValue(item, out valueOfItem);
                if(itemFound)
                {
                    int count = inventoryItems[item].Item2 - 1;
                    inventoryItems[item] = new Tuple<decimal, int>(valueOfItem.Item1, count);
                }
            }

            public bool HasInventoryOfItem(string item)
            {
                InventorySearchResponse response = GetInventoryItem(item);
                if (response.Found == true && response.Invetory > 0)
                    return true;
                else
                    return false;
            }

            public int GetInventoryCountOfItem(string item)
            {
                return GetInventoryItem(item).Invetory;
            }
        }
    }
}
