﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingController
{
    public class AddCoinResponse
    {
        bool success = false;
        public bool Success { get { return success; } }
        CoinTray insertedCoins = new CoinTray();
        public CoinTray InsertedCoins { get { return insertedCoins; } }
        string errorResponse = string.Empty;
        public string ErrorResponse { get { return errorResponse; } }

        public AddCoinResponse(bool _success, CoinTray _insertedCoins, string _errorResponse)
        {
            success = _success;
            insertedCoins = _insertedCoins;
            errorResponse = _errorResponse;
        }

    }
}
