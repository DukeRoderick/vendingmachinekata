﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingController
{
    public partial class VendingMachine
    {
        public const string INSERT_COIN = "INSERT COIN";
        public const string INSERT_COINS = "INSERT COINS";
        public const string EXACT_CHANGE = "EXACT CHANGE ONLY";
        public const string SOLD_OUT = "SOLD OUT";
        public const string THANK_YOU = "THANK YOU";
        public const string PRICE = "PRICE";

        private ChangeHandler changeHandler = null;
        private Inventory inventoryHandler = null;

        public VendingMachine() : this(new CoinTray())
        {

        }

        public VendingMachine(CoinTray startingChange)
        {
            changeHandler = new ChangeHandler(startingChange);
            inventoryHandler = new Inventory();
        }

        public decimal GetTotalStoredValue()
        {
            return changeHandler.StoredChangeRawValue;
        }

        public decimal GetInsertedStoreValue()
        {
            return changeHandler.InsertedChangeRawValue;
        }

        public AddCoinResponse AddChange(VendingChange.ICoin coin)
        {
            return changeHandler.AddChange(coin);
        }

        public CoinTray MakeChange(CoinTray coins, decimal cost)
        {
            return changeHandler.MakeChange(coins, cost);
        }

        public decimal GetDecimalValueOfCoins(CoinTray coins)
        {
            return changeHandler.ValueFromCoinTray(coins);
        }

        public void ReturnChange()
        {
            changeHandler.ReturnCoinsFromInsertedSlot();
        }

        public void EmptyCoinReturn()
        {
            changeHandler.EmptyCoinReturn();
        }

        public void AdjustCoinsAfterPurchase(CoinTray change)
        {
            changeHandler.PurchaseItem(change);
        }

        public CoinTray CheckCoinsInReturnSlot()
        {
            return changeHandler.CheckCoinsInCoinReturn();
        }

        public CoinTray GetCurrenInsertedCoins()
        {
            return changeHandler.GetInsertedCoins();
        }

        public CoinTray GetCurrentStoredCoins()
        {
            return changeHandler.GetStoredCoins();
        }

        public decimal GetPriceOfInventoryItem(string item)
        {
            return inventoryHandler.GetInventoryItem(item).Cost;
        }

        public bool HasInventoryOfItem(string item)
        {
            return inventoryHandler.HasInventoryOfItem(item);
        }

        public void AddToInvetory(string item, int count)
        {
            inventoryHandler.AddInventory(item, count);
        }

        public int GetInventoryOfItem(string item)
        {
            return inventoryHandler.GetInventoryCountOfItem(item);
        }

        public SelectItemResponse PurchaseItem(string item)
        {
            if (GetInventoryOfItem(item) > 0)
            {
                if (GetInsertedStoreValue() >= GetPriceOfInventoryItem(item))
                {
                    CoinTray change = MakeChange(GetCurrenInsertedCoins(), GetPriceOfInventoryItem(item));
                    AdjustCoinsAfterPurchase(change);
                    changeHandler.ReturnCoinsFromInsertedSlot();
                    inventoryHandler.RemoveInvetory(item);
                    return new SelectItemResponse(true, THANK_YOU);
                }
                return new SelectItemResponse(false, PRICE + ": " + GetPriceOfInventoryItem(item));
            }
            else
                return new SelectItemResponse(false, SOLD_OUT);
        }
    }
}
