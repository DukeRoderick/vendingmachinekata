﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingInventory
{
    public class InventorySearchResponse
    {
        private bool found = false;
        private decimal cost = 0m;
        private int inventory = 0;
        public bool Found { get { return found; } }
        public decimal Cost { get { return cost; } }
        public int Invetory { get { return inventory; } }
        public InventorySearchResponse(bool _found, Tuple<decimal, int> item)
        {
            found = _found;
            cost = item.Item1;
            inventory = item.Item2;
        }
    }
}
