﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingInventory
{
    public struct VendingItems
    {
        public const string Cola = "Cola";
        public const string Chips = "Chips";
        public const string Candy = "Candy";
    }
}
