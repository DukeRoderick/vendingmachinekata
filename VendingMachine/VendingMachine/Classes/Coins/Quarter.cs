﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingChange
{
    public struct Quarter : ICoin
    {
        public float Size {  get { return 0.955f; } }
        public float Weight { get { return 5.670f; } }
    }
}
