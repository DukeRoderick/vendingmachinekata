﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingChange
{
    public interface ICoin
    {
        float Size { get; }
        float Weight { get; }
    }
}
