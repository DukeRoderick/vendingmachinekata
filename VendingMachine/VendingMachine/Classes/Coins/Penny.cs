﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingChange
{
    public struct Penny : ICoin
    {
        public float Size { get { return 0.750f; } }
        public float Weight { get { return 2.500f; } }
    }
}
