﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingChange
{
    public struct Nickel : ICoin
    {
        public float Size { get { return 0.835f; } }
        public float Weight { get { return 5.000f; } }
    }
}
